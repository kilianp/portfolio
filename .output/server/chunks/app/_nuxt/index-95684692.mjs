import { b as buildAssetsURL } from '../../handlers/renderer.mjs';
import { useSSRContext, mergeProps } from 'vue';
import { ssrRenderAttrs, ssrRenderComponent, ssrRenderList, ssrRenderClass, ssrInterpolate, ssrRenderAttr } from 'vue/server-renderer';
import { _ as _export_sfc } from '../server.mjs';
import 'vue-bundle-renderer/runtime';
import 'h3';
import 'devalue';
import '../../nitro/node-server.mjs';
import 'node-fetch-native/polyfill';
import 'node:http';
import 'node:https';
import 'destr';
import 'ofetch';
import 'unenv/runtime/fetch/index';
import 'hookable';
import 'scule';
import 'klona';
import 'defu';
import 'ohash';
import 'ufo';
import 'unstorage';
import 'radix3';
import 'node:fs';
import 'node:url';
import 'pathe';
import 'http-graceful-shutdown';
import 'unctx';
import 'vue-router';
import '@unhead/ssr';
import 'unhead';
import '@unhead/shared';

const _imports_0$1 = "" + buildAssetsURL("profile.66de250d.jpg");
const _sfc_main$1 = {};
function _sfc_ssrRender$1(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  _push(`<div${ssrRenderAttrs(mergeProps({ class: "p-8 lg:p-24 lg:pt-16" }, _attrs))}><div class="flex items-center justify-between"><div class="flex items-center" data-aos="fade-right" data-aos-easing="ease-in-sine" data-aos-duration="500"><img${ssrRenderAttr("src", _imports_0$1)} class="h-16 w-16 p-1 rounded-full"><h2 class="text-white font-main font-bold text-xl ml-4">Kilian PICHARD</h2></div><a class="text-second font-main text-xl ml-4 hover:text-white ease-in-out duration-200 hidden lg:inline" href="mailto:pro@kilianp.fr" data-aos="fade-left" data-aos-easing="ease-in-sine" data-aos-duration="500">pro@kilianp.fr</a></div><div class="mt-12 lg:mt-48 flex flex-col lg:flex-row justify-between"><h2 class="text-6xl font-main text-white font-bold relative w-6/12 lg:w-auto">I&#39;m <span class="text-purple">Kilian</span> <span class="emoji absolute top-16 -mt-2 -right-20 lg:-right-20 lg:top-0">\u{1F44B}</span></h2><div class="lg:mr-12 mt-12 lg:mt-0"><h2 class="text-4xl font-main text-white font-semibold">I build Websites and Apps, for all<br> need</h2><h3 class="text-xl text-second mt-2 font-extralight">Freelance <span class="font-bold">FullStack Developper</span> in Paris.</h3></div></div></div>`);
}
const _sfc_setup$1 = _sfc_main$1.setup;
_sfc_main$1.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("components/Header.vue");
  return _sfc_setup$1 ? _sfc_setup$1(props, ctx) : void 0;
};
const __nuxt_component_0 = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["ssrRender", _sfc_ssrRender$1]]);
const _imports_0 = "" + buildAssetsURL("prams-app.e855f3c7.png");
const _imports_1 = "" + buildAssetsURL("geoCarbu.8eef1180.png");
const _imports_2 = "" + buildAssetsURL("militaria.3846bbfe.png");
const _imports_3 = "" + buildAssetsURL("cabane.62f1f7db.png");
const _imports_4 = "" + buildAssetsURL("prams.c1bb281b.png");
const _imports_5 = "" + buildAssetsURL("schuller.53ccf5ee.png");
const _imports_6 = "" + buildAssetsURL("samdoc.6eb93fd7.png");
const _imports_7 = "" + buildAssetsURL("sharinstru.0c8d34d4.png");
const _imports_8 = "" + buildAssetsURL("botify.9f703158.png");
const _imports_9 = "" + buildAssetsURL("sdg.f2432d84.png");
const _imports_10 = "" + buildAssetsURL("samdocLogo.330b20a4.png");
const _sfc_main = {
  name: "IndexPage",
  data: function() {
    return {
      stacks: [
        {
          icon: "devicon-react-plain",
          name: "React"
        },
        {
          icon: "devicon-nuxtjs-plain",
          name: "NuxtJS"
        },
        {
          icon: "devicon-nodejs-plain",
          name: "NodeJS"
        },
        {
          icon: "devicon-vuejs-plain",
          name: "VueJS"
        },
        {
          icon: "devicon-tailwindcss-plain",
          name: "TailwindCSS"
        },
        {
          icon: "devicon-javascript-plain",
          name: "Javascript"
        },
        {
          icon: "devicon-html5-plain",
          name: "HTML"
        },
        {
          icon: "devicon-css3-plain",
          name: "CSS"
        },
        {
          icon: "devicon-mongodb-plain",
          name: "MongoDB"
        }
      ],
      otherStack: [
        {
          icon: "devicon-sass-plain",
          name: "SASS"
        },
        {
          icon: "devicon-swift-plain",
          name: "SwiftUI"
        },
        {
          icon: "devicon-flutter-plain",
          name: "Flutter"
        },
        {
          icon: "devicon-php-plain",
          name: "PHP"
        },
        {
          icon: "devicon-wordpress-plain",
          name: "Wordpress"
        },
        {
          icon: "devicon-vuetify-plain",
          name: "Vuetify"
        },
        {
          icon: "devicon-typescript-plain",
          name: "Typescript"
        },
        {
          icon: "devicon-java-plain",
          name: "Java"
        },
        {
          icon: "devicon-symfony-plain",
          name: "Symfony"
        }
      ],
      toolbox: [
        {
          icon: "devicon-apple-original",
          name: "MacOS / iOS"
        },
        {
          icon: "devicon-express-original",
          name: "Express"
        },
        {
          icon: "devicon-gitlab-plain",
          name: "Gitlab"
        },
        {
          icon: "devicon-heroku-original",
          name: "Heroku"
        },
        {
          icon: "devicon-ionic-original",
          name: "Ionic"
        },
        {
          icon: "devicon-mysql-plain",
          name: "MySQL"
        },
        {
          icon: "devicon-npm-original-wordmark",
          name: "NPM"
        },
        {
          icon: "devicon-docker-plain",
          name: "Docker"
        },
        {
          icon: "devicon-photoshop-plain",
          name: "Adobe Suite"
        }
      ]
    };
  },
  mounted() {
    VANTA.TRUNK({
      el: "#waves",
      mouseControls: true,
      touchControls: true,
      gyroControls: false,
      minHeight: 200,
      minWidth: 200,
      scale: 1,
      scaleMobile: 1,
      spacing: 10,
      color: 3948475,
      backgroundColor: 1052688,
      chaos: 10
    });
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Header = __nuxt_component_0;
  _push(`<div${ssrRenderAttrs(_attrs)}><div class="main">`);
  _push(ssrRenderComponent(_component_Header, null, null, _parent));
  _push(`<div class="w-screen p-6 lg:p-20"><div class="glass w-full p-2 lg:p-12 pb-4 mb-24"><h2 class="text-3xl text-center lg:text-left lg:text-5xl text-white font-bold font-main m-1"> My Main Stack </h2><div class="flex flex-wrap lg:flex-nowrap items-center justify-between lg:p-12 m-2 pt-8"><!--[-->`);
  ssrRenderList(_ctx.stacks, (stack) => {
    _push(`<div class="flex w-4/12 my-4 lg:my-0 lg:w-24 flex-col items-center justify-center"><i class="${ssrRenderClass([stack.icon, "text-5xl align-middle text-white"])}"></i><p class="text-white mt-2 text-xs">${ssrInterpolate(stack.name)}</p></div>`);
  });
  _push(`<!--]--></div><h2 class="text-3xl text-center lg:text-left text-white font-semibold font-main m-1 mt-12"> Other Stack I master </h2><div class="flex flex-wrap lg:flex-nowrap items-center justify-between lg:p-12 m-2 pt-8"><!--[-->`);
  ssrRenderList(_ctx.otherStack, (stack) => {
    _push(`<div class="flex w-4/12 my-4 lg:my-0 lg:w-24 flex-col items-center justify-center"><i class="${ssrRenderClass([stack.icon, "text-5xl align-middle text-white"])}"></i><p class="text-white mt-2 text-xs">${ssrInterpolate(stack.name)}</p></div>`);
  });
  _push(`<!--]--></div><h2 class="text-3xl text-center lg:text-left text-white font-semibold font-main m-1 mt-12"> My ToolBox </h2><div class="flex flex-wrap lg:flex-nowrap items-center justify-between lg:p-12 m-2 pt-8"><!--[-->`);
  ssrRenderList(_ctx.toolbox, (stack) => {
    _push(`<div class="flex w-4/12 my-4 lg:my-0 lg:w-24 flex-col items-center justify-center"><i class="${ssrRenderClass([stack.icon, "text-5xl align-middle text-white"])}"></i><p class="text-white mt-2 text-xs">${ssrInterpolate(stack.name)}</p></div>`);
  });
  _push(`<!--]--></div></div><h2 class="text-5xl text-center lg:text-left text-white font-bold font-main m-1 mb-8"> My Projects </h2><div class="glass projects w-full p-4 lg:p-12 pb-4 lg:mb-24 flex flex-col"><div class="flex flex-col lg:flex-row items-center"><div class="w-12/12 lg:w-7/12" data-aos="slide-up" data-aos-offset="20" data-aos-easing="ease-in-sine" data-aos-duration="300"><img${ssrRenderAttr("src", _imports_0)} data-aos="zoom-out-up" data-aos-offset="-200" data-aos-easing="ease-in-sine" data-aos-duration="500"></div><div class="w-12/12 lg:w-5/12 flex flex-col items-center justify-center" data-aos="fade-zoom-in" data-aos-easing="ease-in-sine" data-aos-duration="600"><h2 class="text-3xl text-white font-bold font-main text-center"> EasyBand </h2><p class="font-main text-white text-lg text-justify my-8"> EasyBand is a react native application that simplifies the management of a music group. It features a calendar of upcoming concerts, setlists, and a library of music. The app also calculates expenses, such as mileage, and includes a feature for conducting polls to gauge member participation in concerts. Additionally, EasyBand has a live feature for displaying lyrics and chords during performances. This all-in-one application is a valuable asset for any music group looking to efficiently manage their events and repertoire </p><div class="flex text-center text-3xl text-white justify-around items-center w-8/12 lg:w-5/12"><i class="devicon-react-original"></i></div></div></div><div class="divider w-full h-1 border-b-white"></div><div class="flex flex-col-reverse lg:flex-row items-center"><div class="w-12/12 lg:w-6/12 flex flex-col items-center justify-center lg:pl-24" data-aos="fade-zoom-in" data-aos-easing="ease-in-sine" data-aos-duration="600"><h2 class="text-3xl text-white font-bold font-main text-center"> G\xC9O CARBU </h2><p class="font-main text-white text-lg text-justify my-8"> The app allows users to find the cheapest gas stations in their area by using their current location. The app displays a list of nearby gas stations and their corresponding prices, allowing users to easily compare prices and choose the cheapest option. The user interface is designed using Swift UI, providing a modern and intuitive experience for the user. Overall, the app helps users save money on gas by finding the best prices in their area. </p><div class="flex text-center text-3xl text-white justify-around items-center w-8/12 lg:w-5/12"><i class="devicon-swift-plain"></i></div><a href="https://gitlab.com/kilianp/geocarbu" target="_blank" class="text-second font-main text-xl hover:text-white ease-in-out duration-200 m-4 border border-main"><i class="devicon-gitlab-plain mr-4"></i>GO TO GITLAB REPO</a></div><div class="w-12/12 lg:w-6/12"><img${ssrRenderAttr("src", _imports_1)} data-aos="zoom-out-up" data-aos-offset="-200" data-aos-easing="ease-in-sine" data-aos-duration="500"></div></div><div class="divider w-full h-1 border-b-white"></div><div class="flex flex-col lg:flex-row items-center"><div class="w-12/12 lg:w-7/12"><img${ssrRenderAttr("src", _imports_2)} data-aos="zoom-out-up" data-aos-offset="-200" data-aos-easing="ease-in-sine" data-aos-duration="500"></div><div class="w-12/12 lg:w-5/12 flex flex-col items-center justify-center" data-aos="fade-zoom-in" data-aos-easing="ease-in-sine" data-aos-duration="600"><h2 class="text-3xl text-white font-bold font-main text-center"> MILITARIA DU BOCAGE </h2><p class="font-main text-white text-lg text-justify my-8"> I completed a website redesign for the company Militaria du Bocage, transitioning from React to WooCommerce. My responsibilities included the entire implementation of the website and secure payment systems, and migrating the database. This project allowed me to showcase my e-commerce development and website customization skills. The redesign was a challenging but rewarding experience, and I was able to apply my web development expertise to a real-world project. </p><div class="flex text-center text-3xl text-white justify-around items-center w-8/12 lg:w-5/12"><i class="devicon-wordpress-plain"></i></div><a href="https://militaria-du-bocage.fr/" target="_blank" class="text-second font-main text-xl hover:text-white ease-in-out duration-200 m-4 border border-main"><i class="devicon-safari-line mr-4"></i>GO TO WEBSITE</a></div></div><div class="divider w-full h-1 border-b-white"></div><div class="flex flex-col-reverse lg:flex-row items-center"><div class="w-12/12 lg:w-6/12 flex flex-col items-center justify-center lg:pl-24" data-aos="fade-zoom-in" data-aos-easing="ease-in-sine" data-aos-duration="600"><h2 class="text-3xl text-white font-bold font-main text-center"> LA CABANE D&#39;ELSANDRE </h2><p class="font-main text-white text-lg text-justify my-8"> I completed a website redesign for the company La Cabane d&#39;elsandre, transitioning from WIX to WooCommerce. I coded the entire website and implemented features such as secure payment systems and a successful database migration. This project allowed me to showcase my e-commerce development skills and was a challenging but rewarding experience. La Cabane d&#39;elsandre specializes in the sale of various decorative items and toys, and I was able to contribute to their online success through the successful completion of this project. </p><div class="flex text-center text-3xl text-white justify-around items-center w-8/12 lg:w-5/12"><i class="devicon-wordpress-plain"></i></div><a href="https://lacabanedelsandre.fr/" target="_blank" class="text-second font-main text-xl hover:text-white ease-in-out duration-200 m-4 border border-main"><i class="devicon-safari-line mr-4"></i>GO TO WEBSITE</a></div><div class="w-12/12 lg:w-6/12"><img${ssrRenderAttr("src", _imports_3)} data-aos="zoom-out-up" data-aos-offset="-200" data-aos-easing="ease-in-sine" data-aos-duration="500"></div></div><div class="divider w-full h-1 border-b-white"></div><div class="flex flex-col lg:flex-row items-center"><div class="w-12/12 lg:w-7/12"><img${ssrRenderAttr("src", _imports_4)} data-aos="zoom-out-up" data-aos-offset="-200" data-aos-easing="ease-in-sine" data-aos-duration="500"></div><div class="w-12/12 lg:w-5/12 flex flex-col items-center justify-center" data-aos="fade-zoom-in" data-aos-easing="ease-in-sine" data-aos-duration="600"><h2 class="text-3xl text-white font-bold font-main text-center"> PRAM&#39;S SHOWCASE WEBSITE </h2><p class="font-main text-white text-lg text-justify my-8"> This website serves as a showcase for my music group, &quot;Pram&#39;s&quot;, where you can access our videos, photos, and information about our musicians/members. This group project was completed in collaboration with <a href="https://julienlp.com" target="_blank">Julien LE PECHEUR</a></p><div class="flex text-center text-3xl text-white justify-around items-center w-8/12 lg:w-5/12"><i class="devicon-vuejs-plain"></i><i class="devicon-gitlab-plain"></i><i class="devicon-heroku-plain"></i></div><a href="https://prams.fr/" target="_blank" class="text-second font-main text-xl hover:text-white ease-in-out duration-200 m-4 border border-main"><i class="devicon-safari-line mr-4"></i>GO TO WEBSITE</a></div></div><div class="divider w-full h-1 border-b-white"></div><div class="flex flex-col-reverse lg:flex-row items-center"><div class="w-12/12 lg:w-6/12 flex flex-col items-center justify-center lg:pl-24" data-aos="fade-zoom-in" data-aos-easing="ease-in-sine" data-aos-duration="600"><h2 class="text-3xl text-white font-bold font-main text-center"> SCHULLER DIGITAL GRAPHIC WEBSITE </h2><p class="font-main text-white text-lg text-justify my-8"> During my work-study program at Schuller Digital Graphic, I was given the opportunity to work on various websites, including the creation of a new website based on the agency&#39;s updated graphic charter. This project allowed me to gain experience with different design effects such as masks and slides. </p><div class="flex text-center text-3xl text-white justify-around items-center w-8/12 lg:w-5/12"><i class="devicon-wordpress-plain"></i><i class="devicon-php-plain"></i><i class="devicon-gitlab-plain"></i></div><a href="https://schuller-digital-graphic.fr/" target="_blank" class="text-second font-main text-xl hover:text-white ease-in-out duration-200 m-4 border border-main"><i class="devicon-safari-line mr-4"></i>GO TO WEBSITE</a></div><div class="w-12/12 lg:w-6/12"><img${ssrRenderAttr("src", _imports_5)} data-aos="zoom-out-up" data-aos-offset="-200" data-aos-easing="ease-in-sine" data-aos-duration="500"></div></div><div class="divider w-full h-1 border-b-white"></div><div class="flex flex-col lg:flex-row items-center"><div class="w-12/12 lg:w-7/12"><img${ssrRenderAttr("src", _imports_6)} data-aos="zoom-out-up" data-aos-offset="-200" data-aos-easing="ease-in-sine" data-aos-duration="500"></div><div class="w-12/12 lg:w-5/12 flex flex-col items-center justify-center" data-aos="fade-zoom-in" data-aos-easing="ease-in-sine" data-aos-duration="600"><h2 class="text-3xl text-white font-bold font-main text-center"> SAMDOC MEDICAL TECHNOLOGIES &#39;CIGAR&#39; </h2><span class="font-main text-white text-lg text-justify my-8"> I completed the finalization and graphic redesign of a CDN manager using Git version control. This web project was built using traditional HTML5, CSS3, and JavaScript. Some of the tasks I performed included adding front/back functionality, redesigning the graphics, and implementing unit tests. </span><div class="flex text-center text-3xl text-white justify-around items-center w-8/12 lg:w-5/12"><i class="devicon-html5-plain"></i><i class="devicon-css3-plain"></i><i class="devicon-javascript-plain"></i><i class="devicon-nodejs-plain"></i><i class="devicon-git-plain"></i></div></div></div><div class="divider w-full h-1 border-b-white"></div><div class="flex flex-col-reverse lg:flex-row items-center"><div class="w-12/12 lg:w-6/12 flex flex-col items-center justify-center lg:pl-24" data-aos="fade-zoom-in" data-aos-easing="ease-in-sine" data-aos-duration="600"><h2 class="text-3xl text-white font-bold font-main text-center"> SHARE INSTRU </h2><p class="font-main text-white text-lg text-justify my-8"> This project was the project of my second year of DUT, it was realized in group. This website was a platform of exchange/loan of instrument from private individual to private individual </p><div class="flex text-center text-3xl text-white justify-around items-center w-8/12 lg:w-5/12"><i class="devicon-html5-plain"></i><i class="devicon-css3-plain"></i><i class="devicon-php-plain"></i><i class="devicon-git-plain"></i></div></div><div class="w-12/12 lg:w-6/12" data-aos="zoom-out-up" data-aos-offset="-200" data-aos-easing="ease-in-sine" data-aos-duration="500"><img${ssrRenderAttr("src", _imports_7)}></div></div></div><h2 class="text-5xl text-center lg:text-left text-white font-bold font-main m-1 mb-8 lg:mt-0 mt-12"> My Experiences </h2><div class="experiences w-full flex flex-col lg:flex-row space-y-6 lg:space-y-0 lg:space-x-12 items-center"><div class="glass w-full lg:w-1/2 aspect-square p-4"><a href="https://botify.com" target="_blank"><h2 class="text-3xl text-white font-bold font-main text-center h-24"> BOTIFY </h2><img class="w-1/3 mx-auto rounded-lg mb-4"${ssrRenderAttr("src", _imports_8)}></a><div class="h-32 flex justify-between flex-col"><p class="text-center text-white font-main"> Development and integration in React &amp; Python for internal tools. </p><p class="text-second font-main text-center">Since August 2022</p></div></div><div class="glass w-full lg:w-1/2 aspect-square p-4"><a href="https://schuller-digital-graphic.fr" target="_blank"><h2 class="text-3xl text-white font-bold font-main text-center h-24"> SCHULLER DIGITAL GRAPHIC </h2><img class="w-1/3 mx-auto rounded-lg mb-4"${ssrRenderAttr("src", _imports_9)}></a><div class="h-32 flex justify-between flex-col"><p class="text-white font-main text-center"> Creation of WordPress websites - Setting up themes - Creating child theme - Deployment </p><p class="text-second font-main text-center"> August 2021 - July 2022 </p></div></div><div class="glass w-full lg:w-1/2 aspect-square p-4"><a href="https://samdoc.io" target="_blank"><h2 class="text-3xl text-white font-bold font-main text-center h-24"> SAMDOC MEDICAL TECHNOLOGIES </h2><img class="w-1/3 mx-auto rounded-lg mb-4"${ssrRenderAttr("src", _imports_10)}></a><div class="h-32 flex justify-between flex-col"><p class="text-white font-main text-center"> I redesigned and developed the internal C.I.G.A.R. application. This included updating the user interface, adding new features, and optimizing the overall performance of the application </p><p class="text-second font-main text-center">April - June 2021</p></div></div></div></div></div><div class="bg-main test w-screen h-screen fixed top-0 left-0"><div class="h-full w-full opacity-30" id="waves"></div></div></div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/index.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const index = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);

export { index as default };
//# sourceMappingURL=index-95684692.mjs.map
