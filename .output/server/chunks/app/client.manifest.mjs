const client_manifest = {
  "assets/img/botify.png": {
    "resourceType": "image",
    "mimeType": "image/png",
    "file": "botify.9f703158.png",
    "src": "assets/img/botify.png"
  },
  "assets/img/cabane.png": {
    "resourceType": "image",
    "mimeType": "image/png",
    "file": "cabane.62f1f7db.png",
    "src": "assets/img/cabane.png"
  },
  "assets/img/geoCarbu.png": {
    "resourceType": "image",
    "mimeType": "image/png",
    "file": "geoCarbu.8eef1180.png",
    "src": "assets/img/geoCarbu.png"
  },
  "assets/img/militaria.png": {
    "resourceType": "image",
    "mimeType": "image/png",
    "file": "militaria.3846bbfe.png",
    "src": "assets/img/militaria.png"
  },
  "assets/img/prams-app.png": {
    "resourceType": "image",
    "mimeType": "image/png",
    "file": "prams-app.e855f3c7.png",
    "src": "assets/img/prams-app.png"
  },
  "assets/img/prams.png": {
    "resourceType": "image",
    "mimeType": "image/png",
    "file": "prams.c1bb281b.png",
    "src": "assets/img/prams.png"
  },
  "assets/img/profile.jpg": {
    "resourceType": "image",
    "mimeType": "image/jpeg",
    "file": "profile.66de250d.jpg",
    "src": "assets/img/profile.jpg"
  },
  "assets/img/samdoc.png": {
    "resourceType": "image",
    "mimeType": "image/png",
    "file": "samdoc.6eb93fd7.png",
    "src": "assets/img/samdoc.png"
  },
  "assets/img/samdocLogo.png": {
    "resourceType": "image",
    "mimeType": "image/png",
    "file": "samdocLogo.330b20a4.png",
    "src": "assets/img/samdocLogo.png"
  },
  "assets/img/schuller.png": {
    "resourceType": "image",
    "mimeType": "image/png",
    "file": "schuller.53ccf5ee.png",
    "src": "assets/img/schuller.png"
  },
  "assets/img/sdg.png": {
    "resourceType": "image",
    "mimeType": "image/png",
    "file": "sdg.f2432d84.png",
    "src": "assets/img/sdg.png"
  },
  "assets/img/sharinstru.png": {
    "resourceType": "image",
    "mimeType": "image/png",
    "file": "sharinstru.0c8d34d4.png",
    "src": "assets/img/sharinstru.png"
  },
  "node_modules/@nuxt/ui-templates/dist/templates/error-404.css": {
    "resourceType": "style",
    "file": "error-404.8bdbaeb8.css",
    "src": "node_modules/@nuxt/ui-templates/dist/templates/error-404.css"
  },
  "node_modules/@nuxt/ui-templates/dist/templates/error-404.vue": {
    "resourceType": "script",
    "module": true,
    "css": [],
    "file": "error-404.df2724e5.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "isDynamicEntry": true,
    "src": "node_modules/@nuxt/ui-templates/dist/templates/error-404.vue"
  },
  "error-404.8bdbaeb8.css": {
    "file": "error-404.8bdbaeb8.css",
    "resourceType": "style"
  },
  "node_modules/@nuxt/ui-templates/dist/templates/error-500.css": {
    "resourceType": "style",
    "file": "error-500.b63a96f5.css",
    "src": "node_modules/@nuxt/ui-templates/dist/templates/error-500.css"
  },
  "node_modules/@nuxt/ui-templates/dist/templates/error-500.vue": {
    "resourceType": "script",
    "module": true,
    "css": [],
    "file": "error-500.50102118.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "isDynamicEntry": true,
    "src": "node_modules/@nuxt/ui-templates/dist/templates/error-500.vue"
  },
  "error-500.b63a96f5.css": {
    "file": "error-500.b63a96f5.css",
    "resourceType": "style"
  },
  "node_modules/nuxt/dist/app/entry.css": {
    "resourceType": "style",
    "file": "entry.73168167.css",
    "src": "node_modules/nuxt/dist/app/entry.css"
  },
  "node_modules/nuxt/dist/app/entry.js": {
    "resourceType": "script",
    "module": true,
    "css": [
      "entry.73168167.css"
    ],
    "dynamicImports": [
      "node_modules/@nuxt/ui-templates/dist/templates/error-404.vue",
      "node_modules/@nuxt/ui-templates/dist/templates/error-500.vue"
    ],
    "file": "entry.6f68a836.js",
    "isEntry": true,
    "src": "node_modules/nuxt/dist/app/entry.js"
  },
  "entry.73168167.css": {
    "file": "entry.73168167.css",
    "resourceType": "style"
  },
  "pages/index.css": {
    "resourceType": "style",
    "file": "index.b4ced7ec.css",
    "src": "pages/index.css"
  },
  "pages/index.vue": {
    "resourceType": "script",
    "module": true,
    "assets": [
      "profile.66de250d.jpg",
      "prams-app.e855f3c7.png",
      "geoCarbu.8eef1180.png",
      "militaria.3846bbfe.png",
      "cabane.62f1f7db.png",
      "prams.c1bb281b.png",
      "schuller.53ccf5ee.png",
      "samdoc.6eb93fd7.png",
      "sharinstru.0c8d34d4.png",
      "botify.9f703158.png",
      "sdg.f2432d84.png",
      "samdocLogo.330b20a4.png"
    ],
    "css": [],
    "file": "index.adffe591.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "isDynamicEntry": true,
    "src": "pages/index.vue"
  },
  "index.b4ced7ec.css": {
    "file": "index.b4ced7ec.css",
    "resourceType": "style"
  },
  "profile.66de250d.jpg": {
    "file": "profile.66de250d.jpg",
    "resourceType": "image",
    "mimeType": "image/jpeg"
  },
  "prams-app.e855f3c7.png": {
    "file": "prams-app.e855f3c7.png",
    "resourceType": "image",
    "mimeType": "image/png"
  },
  "geoCarbu.8eef1180.png": {
    "file": "geoCarbu.8eef1180.png",
    "resourceType": "image",
    "mimeType": "image/png"
  },
  "militaria.3846bbfe.png": {
    "file": "militaria.3846bbfe.png",
    "resourceType": "image",
    "mimeType": "image/png"
  },
  "cabane.62f1f7db.png": {
    "file": "cabane.62f1f7db.png",
    "resourceType": "image",
    "mimeType": "image/png"
  },
  "prams.c1bb281b.png": {
    "file": "prams.c1bb281b.png",
    "resourceType": "image",
    "mimeType": "image/png"
  },
  "schuller.53ccf5ee.png": {
    "file": "schuller.53ccf5ee.png",
    "resourceType": "image",
    "mimeType": "image/png"
  },
  "samdoc.6eb93fd7.png": {
    "file": "samdoc.6eb93fd7.png",
    "resourceType": "image",
    "mimeType": "image/png"
  },
  "sharinstru.0c8d34d4.png": {
    "file": "sharinstru.0c8d34d4.png",
    "resourceType": "image",
    "mimeType": "image/png"
  },
  "botify.9f703158.png": {
    "file": "botify.9f703158.png",
    "resourceType": "image",
    "mimeType": "image/png"
  },
  "sdg.f2432d84.png": {
    "file": "sdg.f2432d84.png",
    "resourceType": "image",
    "mimeType": "image/png"
  },
  "samdocLogo.330b20a4.png": {
    "file": "samdocLogo.330b20a4.png",
    "resourceType": "image",
    "mimeType": "image/png"
  }
};

export { client_manifest as default };
//# sourceMappingURL=client.manifest.mjs.map
