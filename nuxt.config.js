// https://v3.nuxtjs.org/docs/directory-structure/nuxt.config
export default defineNuxtConfig({
  ssr: true,
  app: {
    head: {
      title: "Kilian PICHARD - FullStack Developper",
      htmlAttrs: {
        lang: "fr",
      },
      meta: [
        { charset: "utf-8" },
        {
          name: "viewport",
          content:
            'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"',
        },
        { hid: "description", name: "description", content: "" },
        { name: "format-detection", content: "telephone=no" },
      ],
      link: [
        { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
        { rel: "preconnect", href: "https://fonts.googleapis.com" },
        { rel: "preconnect", href: "https://fonts.gstatic.com" },
        {
          rel: "stylesheet",
          href: "https://fonts.googleapis.com/css2?family=Outfit:wght@100;200;300;400;500;600;700;800;900&display=swap",
        },
        {
          rel: "stylesheet",
          href: "https://cdn.jsdelivr.net/gh/devicons/devicon@v2.15.1/devicon.min.css",
        },
      ],
      script: [
        { src: "js/three.min.js" },
        { src: "js/p5.min.js" },
        { src: "js/vanta.waves.min.js" },
      ],
    },
  },

  css: ["@/assets/css/main.css"],

  plugins: [
    "~/plugins/aos.client.js", // In Nuxt 3, you specify the mode as part of the plugin filename.
  ],

  components: true,

  modules: ["@nuxtjs/tailwindcss"],

  publicRuntimeConfig: {
    postcss: {
      postcssOptions: {
        plugins: {
          tailwindcss: {},
          autoprefixer: {},
        },
      },
    },
  },
});
