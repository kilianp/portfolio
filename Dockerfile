FROM node:16-alpine

WORKDIR /usr/src/app

COPY . .

CMD [ "yarn", "start" ]
HEALTHCHECK --interval=1m --timeout=20s --retries=3 CMD curl --fail http://localhost:$PORT || exit 1
